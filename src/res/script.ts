/** Copyright © 2017 Fabio Iotti. Released under the terms of MIT license. */
namespace ColorPickerScript {

	class RGBSpaceColor {

		constructor(
			/** Range [0, 255] */
			public r: number = 0,
			/** Range [0, 255] */
			public g: number = 0,
			/** Range [0, 255] */
			public b: number = 0
		) {
			this.r = Math.max(0, Math.min(r, 255));
			this.g = Math.max(0, Math.min(g, 255));
			this.b = Math.max(0, Math.min(b, 255));
		}

		public toHSV(): HSVSpaceColor {
			var red = this.r/255;
			var green = this.g/255;
			var blue = this.b/255;
			
			var h, s, v, max, min, d;

			red = red > 1 ? 1 : red < 0 ? 0 : red;
			green = green > 1 ? 1 : green < 0 ? 0 : green;
			blue = blue > 1 ? 1 : blue < 0 ? 0 : blue;

			max = min = red;
			
			if (green > max)
				max = green;
			
			if (green < min)
				min = green;
			
			if (blue > max)
				max = blue;
			
			if (blue < min)
				min = blue;
			
			d = max - min;
			v = max;
			
			s = (max > 0) ? d/max : 0;

			if (s == 0) {
				h = 0;
			} else {
				h = 60 * ((red == max) ? (green-blue)/d : ((green==max) ? 2+(blue-red)/d : 4+(red-green)/d));
				
				if (h < 0)
					h += 360;
			}
			
			return new HSVSpaceColor(h, s * 100, v * 100);
		}

		public toString(): string {
			return `rgb(${Math.round(this.r)}, ${Math.round(this.g)}, ${Math.round(this.b)})`;
		}
	}

	class HSVSpaceColor {

		constructor(
			/** Range [0, 360] */
			public h: number = 0,
			/** Range [0, 100] */
			public s: number = 0,
			/** Range [0, 100] */
			public v: number = 0
		) {
			this.h = (h % 360 + 360) % 360;
			this.s = Math.max(0, Math.min(s, 100));
			this.v = Math.max(0, Math.min(v, 100));
		}

		public toHSL(): HSLSpaceColor {
			var t = (2 - this.s/100) * this.v/100;
			var saturation = this.s/100 * this.v/100 / (t < 1 ? t : 2 - t) * 100;
			return new HSLSpaceColor(this.h, (isNaN(saturation) ? 0 : saturation), t * 50);
		}

		public toString(): string {
			return `hsv(${Math.round(this.h)}, ${Math.round(this.s)}%, ${Math.round(this.v)}%)`;
		}
	}

	class HSLSpaceColor {

		constructor(
			/** Range [0, 360] */
			public h: number = 0,
			/** Range [0, 100] */
			public s: number = 0,
			/** Range [0, 100] */
			public l: number = 0
		) {
			this.h = (h % 360 + 360) % 360;
			this.s = Math.max(0, Math.min(s, 100));
			this.l = Math.max(0, Math.min(l, 100));
		}

		public toRGB(): RGBSpaceColor {
			const rgb = HSLSpaceColor.hsl2rgb(this.h, this.s, this.l);

			return new RGBSpaceColor(rgb.r, rgb.g, rgb.b);
		}

		public toString(): string {
			return `hsl(${Math.round(this.h)}, ${Math.round(this.s)}%, ${Math.round(this.l)}%)`;
		}

		private static hsl2rgb(h: number, s: number, l: number){function f(s: number, l: number, c: number){0>c?c+=1:1<c&&(c-=1);return 255*(1>6*c?s+6*(l-s)*c:1>2*c?l:2>3*c?s+6*(l-s)*(2/3-c):s)}l/=100;var e,g;0==s?h=g=s=255*l:(s/=100,s=0.5>=l?l*(s+1):l+s-l*s,l=2*l-s,e=h/360,h=f(l,s,e+1/3),g=f(l,s,e),s=f(l,s,e-1/3));return{r:h,g:g,b:s}}
	}

	type Color = number;
	function Color(r: number, g: number, b: number): Color {
		return (
			Math.max(0, Math.min(255, Math.round(b)))       |
			Math.max(0, Math.min(255, Math.round(g))) <<  8 |
			Math.max(0, Math.min(255, Math.round(r))) << 16
		);
	}
	namespace Color {

		export const Standard: { [name: string]: Color } = {
			AliceBlue: 0xf0f8ff,
			AntiqueWhite: 0xfaebd7,
			Aqua: 0x00ffff,
			Aquamarine: 0x7fffd4,
			Azure: 0xf0ffff,
			Beige: 0xf5f5dc,
			Bisque: 0xffe4c4,
			Black: 0x000000,
			BlanchedAlmond: 0xffebcd,
			Blue: 0x0000ff,
			BlueViolet: 0x8a2be2,
			Brown: 0xa52a2a,
			BurlyWood: 0xdeb887,
			CadetBlue: 0x5f9ea0,
			Chartreuse: 0x7fff00,
			Chocolate: 0xd2691e,
			Coral: 0xff7f50,
			CornflowerBlue: 0x6495ed,
			Cornsilk: 0xfff8dc,
			Crimson: 0xdc143c,
			Cyan: 0x00ffff,
			DarkBlue: 0x00008b,
			DarkCyan: 0x008b8b,
			DarkGoldenRod: 0xb8860b,
			DarkGray: 0xa9a9a9,
			DarkGreen: 0x006400,
			DarkKhaki: 0xbdb76b,
			DarkMagenta: 0x8b008b,
			DarkOliveGreen: 0x556b2f,
			DarkOrange: 0xff8c00,
			DarkOrchid: 0x9932cc,
			DarkRed: 0x8b0000,
			DarkSalmon: 0xe9967a,
			DarkSeaGreen: 0x8fbc8f,
			DarkSlateBlue: 0x483d8b,
			DarkSlateGray: 0x2f4f4f,
			DarkTurquoise: 0x00ced1,
			DarkViolet: 0x9400d3,
			DeepPink: 0xff1493,
			DeepSkyBlue: 0x00bfff,
			DimGray: 0x696969,
			DodgerBlue: 0x1e90ff,
			FireBrick: 0xb22222,
			FloralWhite: 0xfffaf0,
			ForestGreen: 0x228b22,
			Fuchsia: 0xff00ff,
			Gainsboro: 0xdcdcdc,
			GhostWhite: 0xf8f8ff,
			Gold: 0xffd700,
			GoldenRod: 0xdaa520,
			Gray: 0x808080,
			Green: 0x008000,
			GreenYellow: 0xadff2f,
			HoneyDew: 0xf0fff0,
			HotPink: 0xff69b4,
			IndianRed : 0xcd5c5c,
			Indigo : 0x4b0082,
			Ivory: 0xfffff0,
			Khaki: 0xf0e68c,
			Lavender: 0xe6e6fa,
			LavenderBlush: 0xfff0f5,
			LawnGreen: 0x7cfc00,
			LemonChiffon: 0xfffacd,
			LightBlue: 0xadd8e6,
			LightCoral: 0xf08080,
			LightCyan: 0xe0ffff,
			LightGoldenRodYellow: 0xfafad2,
			LightGray: 0xd3d3d3,
			LightGreen: 0x90ee90,
			LightPink: 0xffb6c1,
			LightSalmon: 0xffa07a,
			LightSeaGreen: 0x20b2aa,
			LightSkyBlue: 0x87cefa,
			LightSlateGray: 0x778899,
			LightSteelBlue: 0xb0c4de,
			LightYellow: 0xffffe0,
			Lime: 0x00ff00,
			LimeGreen: 0x32cd32,
			Linen: 0xfaf0e6,
			Magenta: 0xff00ff,
			Maroon: 0x800000,
			MediumAquaMarine: 0x66cdaa,
			MediumBlue: 0x0000cd,
			MediumOrchid: 0xba55d3,
			MediumPurple: 0x9370db,
			MediumSeaGreen: 0x3cb371,
			MediumSlateBlue: 0x7b68ee,
			MediumSpringGreen: 0x00fa9a,
			MediumTurquoise: 0x48d1cc,
			MediumVioletRed: 0xc71585,
			MidnightBlue: 0x191970,
			MintCream: 0xf5fffa,
			MistyRose: 0xffe4e1,
			Moccasin: 0xffe4b5,
			NavajoWhite: 0xffdead,
			Navy: 0x000080,
			OldLace: 0xfdf5e6,
			Olive: 0x808000,
			OliveDrab: 0x6b8e23,
			Orange: 0xffa500,
			OrangeRed: 0xff4500,
			Orchid: 0xda70d6,
			PaleGoldenRod: 0xeee8aa,
			PaleGreen: 0x98fb98,
			PaleTurquoise: 0xafeeee,
			PaleVioletRed: 0xdb7093,
			PapayaWhip: 0xffefd5,
			PeachPuff: 0xffdab9,
			Peru: 0xcd853f,
			Pink: 0xffc0cb,
			Plum: 0xdda0dd,
			PowderBlue: 0xb0e0e6,
			Purple: 0x800080,
			Red: 0xff0000,
			RosyBrown: 0xbc8f8f,
			RoyalBlue: 0x4169e1,
			SaddleBrown: 0x8b4513,
			Salmon: 0xfa8072,
			SandyBrown: 0xf4a460,
			SeaGreen: 0x2e8b57,
			SeaShell: 0xfff5ee,
			Sienna: 0xa0522d,
			Silver: 0xc0c0c0,
			SkyBlue: 0x87ceeb,
			SlateBlue: 0x6a5acd,
			SlateGray: 0x708090,
			Snow: 0xfffafa,
			SpringGreen: 0x00ff7f,
			SteelBlue: 0x4682b4,
			Tan: 0xd2b48c,
			Teal: 0x008080,
			Thistle: 0xd8bfd8,
			Tomato: 0xff6347,
			Turquoise: 0x40e0d0,
			Violet: 0xee82ee,
			Wheat: 0xf5deb3,
			White: 0xffffff,
			WhiteSmoke: 0xf5f5f5,
			Yellow: 0xffff00,
			YellowGreen: 0x9acd32
		};

		const StandardPositions: { [name: string]: { x: number, y: number, z: number } } = {};
		for (let name in Standard)
			StandardPositions[name] = positionOf(toHSV(Standard[name]));

		export function r(color: Color): number { return (color & 0xff0000) >> 16; }
		export function g(color: Color): number { return (color & 0x00ff00) >> 8; }
		export function b(color: Color): number { return (color & 0x0000ff) >> 0; }

		export function toRGB(color: Color): RGBSpaceColor { return new RGBSpaceColor(r(color), g(color), b(color)); }
		export function toHSV(color: Color): HSVSpaceColor { return toRGB(color).toHSV(); }
		export function toHSL(color: Color): HSLSpaceColor { return toHSV(color).toHSL(); }

		export function toString(color: Color): string {
			return `#${("000000" + (color & 0xffffff).toString(16)).substr(-6)}`;
		}

		export function parse(color: string): Color|null {
			var uc = color.toUpperCase();
			for (let name in Standard)
				if (name.toUpperCase() == uc)
					return Standard[name];
			
			if (/^\s*#([0-9a-fA-F]{3}|[0-9a-fA-F]{6}$)\s*/.test(color)) {
				if (color.length == 7) {
					let parsed = parseInt(color.substring(1), 16);
					if (!isNaN(parsed))
						return parsed;
				} else if (color.length == 4) {
					let x = parseInt(color.substring(1), 16);
					return ((x & 0x00f) <<  0 | (x & 0x00f) <<  4) |  // 0x00f => 0x00000f | 0x0000f0
						((x & 0x0f0) <<  4 | (x & 0x0f0) <<  8) |  // 0x0f0 => 0x000f00 | 0x00f000
						((x & 0xf00) <<  8 | (x & 0xf00) << 12);   // 0xf00 => 0x0f0000 | 0xf00000
				}
			}

			var match = /^\s*rgb\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*\)\s*$/.exec(color);
			if (match)
				return Color(parseInt(match[1]), parseInt(match[2]), parseInt(match[3]));

			match = /^\s*hsl\s*\(\s*([0-9]+)\s*,\s*([0-9]+)%\s*,\s*([0-9]+)%\s*\)\s*$/.exec(color);
			if (match) {
				let rgb = new HSLSpaceColor(parseInt(match[1]), parseInt(match[2]), parseInt(match[3])).toRGB();
				return Color(rgb.r, rgb.g, rgb.b);
			}

			match = /^\s*hsv\s*\(\s*([0-9]+)\s*,\s*([0-9]+)%\s*,\s*([0-9]+)%\s*\)\s*$/.exec(color);
			if (match) {
				let rgb = new HSVSpaceColor(parseInt(match[1]), parseInt(match[2]), parseInt(match[3])).toHSL().toRGB();
				return Color(rgb.r, rgb.g, rgb.b);
			}

			return null;
		}

		export function getName(color: Color): string {
			for (let name in Standard)
				if (Standard[name] == color)
					return name;

			return toString(color);
		}

		export function getMostSimilarStandardColor(color: Color): Color {
			var bestName = 'black';
			var bestDistance = Number.MAX_VALUE;

			var colorPosition = positionOf(toHSV(color));

			for (let name in Standard) {
				let ndist = vectorDistanceSquare(colorPosition, StandardPositions[name]);
				if (ndist < bestDistance) {
					bestDistance = ndist;
					bestName = name;
				}
			}
			
			return Standard[bestName];
		}

		function vectorDistanceSquare(c1: { x: number, y: number, z: number }, c2: { x: number, y: number, z: number }) {
			return (c1.x - c2.x)*(c1.x - c2.x) + (c1.y - c2.y)*(c1.y - c2.y) + (c1.z - c2.z)*(c1.z - c2.z);
		}

		function positionOf(color: HSVSpaceColor) {
			return {
				x: Math.cos(color.h * Math.PI/180) * color.s * color.v,
				y: Math.sin(color.h * Math.PI/180) * color.s * color.v,
				z: color.v * 100
			};
		}
	}

	/** Listen for one-finger touch on an element, returns a function to stop listening. */
	function listenForTouch(el: HTMLElement, cb: (touch: Touch) => void): () => void {
		var activeTouch: number|undefined;
		function getCurrentTouch(e: TouchEvent): Touch|null {
			for (let i = 0; i < e.changedTouches.length; i++)
				if (e.changedTouches[i].identifier == activeTouch)
					return e.changedTouches[i];

			return null;
		}

		function touchStart(e: TouchEvent): void {
			if (activeTouch != undefined)
				return;
			
			e.preventDefault();

			activeTouch = e.changedTouches[0].identifier;
			window.addEventListener('touchend', touchEnd);
			window.addEventListener('touchmove', touchMove);

			touchMove(e);
		}

		function touchMove(e: TouchEvent): void {
			let currentTouch = getCurrentTouch(e);
			if (!currentTouch)
				return;

			e.preventDefault();
			
			cb(currentTouch);
		}

		function touchEnd(e: TouchEvent): void {
			let currentTouch = getCurrentTouch(e);
			if (!currentTouch)
				return;

			e.preventDefault();

			try {
				touchMove(e);
			} finally {
				activeTouch = undefined;
				window.removeEventListener('touchend', touchEnd);
				window.removeEventListener('touchmove', touchMove);
			}
		}

		el.addEventListener('touchstart', touchStart);

		return () => {
			el.removeEventListener('touchstart', touchStart);
		};
	}

	/** Listen for mouse click and movement on an element, returns a function to stop listening. */
	function listenForMouse(el: HTMLElement, cb: (event: MouseEvent) => void): () => void {
		function mouseDown(e: MouseEvent) {
			e.preventDefault();

			window.addEventListener('mouseup', mouseUp);
			window.addEventListener('mousemove', mouseMove);

			cb(e);
		}

		function mouseMove(e: MouseEvent): void {
			e.preventDefault();

			cb(e);
		};

		function mouseUp(e: MouseEvent): void {
			e.preventDefault();

			mouseMove(e);

			window.removeEventListener('mouseup', mouseUp);
			window.removeEventListener('mousemove', mouseMove);

			cb(e);
		};

		el.addEventListener('mousedown', mouseDown);

		return () => {
			el.removeEventListener('mousedown', mouseDown);
		};
	}

	function copyToClipboard(text: string): void {
		// TODO: save currently focused element and selection

		var input = document.createElement('input');
		input.value = text;
		document.body.appendChild(input);
		input.select();
		
		try {
			document.execCommand('copy');
		} finally {
			document.body.removeChild(input);

			// TODO: restore previously focused element and selection
		}
	}

	class ColorPicker {

		public onChangeColor: (source: ColorPicker) => void;
		public onSelect: (source: ColorPicker, value: string) => void;

		constructor(
			private container: HTMLElement,
			private pointer: HTMLElement,
			private saturationBack: HTMLElement,
			private saturationBar: HTMLElement,
			private saturationPointer: HTMLElement,
			private inputControls: HTMLElement,
			private colorRGB: HTMLElement,
			private colorHSL: HTMLElement,
			private colorHSV: HTMLElement,
			private colorHEX: HTMLElement,
			private colorName: HTMLElement
		) {
			this.setHSV(new HSVSpaceColor(0, 100, 100));

			this.registerContainer();

			for (let control of [colorRGB, colorHSL, colorHSV, colorHEX, colorName]) {
				control.addEventListener('mousedown', e => e.stopPropagation());
				control.addEventListener('click', e => {
					e.preventDefault();
					
					if (this.onSelect)
						this.onSelect(this, control.textContent as string);
				});
			}
		}

		public getHSV(): HSVSpaceColor {
			return new HSVSpaceColor(
				parseFloat(this.pointer.style.left || "0") / 100 * 360,
				100 - parseFloat(this.saturationPointer.style.top || "0"),
				100 - parseFloat(this.pointer.style.top || "0")
			);
		}

		public setHSV(hsvColor: HSVSpaceColor): void {
			const hslColor = hsvColor.toHSL();
			const rgbColor = hslColor.toRGB();
			const color = Color(rgbColor.r, rgbColor.g, rgbColor.b);
			const standard = Color.getMostSimilarStandardColor(color);

			this.pointer.style.left = `${hsvColor.h * 100 / 360}%`;
			this.pointer.style.top = `${100 - hsvColor.v}%`;
			this.pointer.style.backgroundColor = `${rgbColor}`;

			this.saturationBack.style.opacity = `${1 - hsvColor.s / 100}`;

			this.saturationBar.style.background = `linear-gradient(${new HSVSpaceColor(hsvColor.h, 100, hsvColor.v).toHSL()} 0%, ${new HSVSpaceColor(hsvColor.h, 0, hsvColor.v).toHSL()} 100%)`;

			this.saturationPointer.style.top = `${100 - hsvColor.s}%`;
			this.saturationPointer.style.backgroundColor = `${rgbColor}`;

			this.inputControls.classList.add(hsvColor.h < 180 ? "controls-right" : "controls-left");
			this.inputControls.classList.remove(hsvColor.h < 180 ? "controls-left" : "controls-right");

			let controls = this.inputControls.querySelectorAll('.control');
			for (let i = 0; i < controls.length; i++)
				(controls[i] as HTMLElement).style.backgroundColor = `${rgbColor}`;
			
			this.colorRGB.textContent = `${rgbColor}`;
			this.colorHSL.textContent = `${hslColor}`;
			this.colorHSV.textContent = `${hsvColor}`;
			this.colorHEX.textContent = `${Color.toString(color)}`;
			this.colorName.textContent = `${Color.getName(standard)}`;

			this.colorName.style.backgroundColor = `${Color.toString(standard)}`;

			if (this.onChangeColor)
				this.onChangeColor(this);
		}

		/** Start listening for mouse or touch on `container`. */
		private registerContainer(): void {
			const repositionPointer = (e: { clientX: number, clientY: number }): void => {
				let rect = this.container.getBoundingClientRect();
				let x = (e.clientX - rect.left) / this.container.clientWidth;
				let y = (e.clientY - rect.top) / this.container.clientHeight;

				const currentColor = this.getHSV();
				this.setHSV(
					new HSVSpaceColor(
						Math.max(0, Math.min(360, 360 * x)),
						currentColor.s,
						100 - Math.max(0, Math.min(100, 100 * y))
					)
				);
			};

			listenForTouch(this.container, repositionPointer);
			listenForMouse(this.container, repositionPointer);

			const repositionSaturationPointer = (e: { clientY: number }): void => {
				let rect = this.saturationBar.getBoundingClientRect();
				let y = (e.clientY - rect.top) / this.container.clientHeight;

				const currentColor = this.getHSV();
				this.setHSV(
					new HSVSpaceColor(
						currentColor.h,
						100 - Math.max(0, Math.min(100, 100 * y)),
						currentColor.v
					)
				);
			};

			listenForTouch(this.saturationBar, repositionSaturationPointer);
			listenForMouse(this.saturationBar, repositionSaturationPointer);
		}
	}

	export function run(): void {
		const cp = new ColorPicker(
			document.getElementById('container') as HTMLElement,
			document.getElementById('pointer') as HTMLElement,
			document.getElementById('saturationBack') as HTMLElement,
			document.getElementById('saturationBar') as HTMLElement,
			document.getElementById('saturationPointer') as HTMLElement,
			document.getElementById('inputControls') as HTMLElement,
			document.getElementById('colorRGB') as HTMLElement,
			document.getElementById('colorHSL') as HTMLElement,
			document.getElementById('colorHSV') as HTMLElement,
			document.getElementById('colorHEX') as HTMLElement,
			document.getElementById('colorName') as HTMLElement
		);

		var setHashTimeoutID: number;
		function setHash(value: string): void {
			if (setHashTimeoutID)
				clearTimeout(setHashTimeoutID);

			setHashTimeoutID = setTimeout(() => {
				location.replace(`#${value.replace(/^#/, '')}`);
			}, 20);
		}

		var lastHash: string;
		var lastColor: Color;

		cp.onChangeColor = (cp) => {
			const newColorRGB = cp.getHSV().toHSL().toRGB();
			const newColor = Color(newColorRGB.r, newColorRGB.g, newColorRGB.b);

			if (newColor == lastColor)
				return;

			lastColor = newColor;
			lastHash = Color.toString(newColor);
			setHash(lastHash);
		};

		cp.onSelect = (cp, value) => {
			copyToClipboard(value);

			lastHash = value.replace(/\s/g, '').toLowerCase();
			setHash(lastHash);
		};

		function hashChanged(): void {
			if (location.hash == lastHash)
				return;
			
			let parsed = Color.parse(location.hash.substr(1));  // try parsing without initial hash (example: `#red`)
			if (parsed == null)
				parsed = Color.parse(location.hash);  // try parsing with initial hash (example: `#ff0000`)
			
			if (parsed != null) {
				lastColor = parsed;
				cp.setHSV(new RGBSpaceColor(Color.r(parsed), Color.g(parsed), Color.b(parsed)).toHSV());
			}
		}

		window.addEventListener('hashchange', hashChanged);
		hashChanged();
	}
}

ColorPickerScript.run();
