const del = require("del");
const gulp = require("gulp");
const gulpLess = require("gulp-less");
const gulpSourcemaps = require("gulp-sourcemaps");
const gulpTslint = require("gulp-tslint").default;
const gulpTypescript = require("gulp-typescript");
const gulpUglify = require("gulp-uglify");
const gulpUglifycss = require("gulp-uglifycss");
const tslint = require("tslint");

function handleError(error) {
	// NOTE: here we must handle errors to avoid some plugins hanging forever.
	console.error(`Error ${error.plugin} ${error.filename}(${error.line};${error.column||0}): ${error.message}`);
	this.emit("end");
}

const tsProject = gulpTypescript.createProject("tsconfig.json");
gulp.task("typescript", () =>
	gulp.src(["src/**/*.ts"], { base: "src" })
		.pipe(gulpSourcemaps.init())
		.pipe(tsProject())
		.pipe(gulpSourcemaps.write(".", { includeContent: false, sourceRoot: "../src" }))
		.pipe(gulp.dest("dist"))
);

gulp.task("typescript-dist", () =>
	gulp.src(["src/**/*.ts"], { base: "src" })
		.pipe(tsProject())
		.pipe(gulpUglify())
		.on("error", handleError)
		.pipe(gulp.dest("dist"))
);

gulp.task("less", () =>
	gulp.src(["src/**/*.less"], { base: "src" })
		.pipe(gulpSourcemaps.init())
		.pipe(gulpLess())
		.on("error", handleError)
		.pipe(gulpSourcemaps.write(".", { includeContent: false, sourceRoot: "../src" }))
		.pipe(gulp.dest("dist"))
);

gulp.task("less-dist", () =>
	gulp.src(["src/**/*.less"], { base: "src" })
		.pipe(gulpLess())
		.on("error", handleError)
		.pipe(gulpUglifycss())
		.pipe(gulp.dest("dist"))
);

gulp.task("static", () =>
	gulp.src(["src/**/*", "!**/*.ts", "!**/*.less"])
		.pipe(gulp.dest("dist"))
);

gulp.task("clean", () => del("dist/**/*"));

gulp.task("build", ["typescript", "less", "static"]);

gulp.task("build-dist", ["clean", "typescript-dist", "less-dist", "static"]);

gulp.task("watch", ["build"], (cb) => {
	gulp.watch(["src/**/*.ts"], ["typescript"]);
	gulp.watch(["src/**/*.less"], ["less"]);

	// Unoptimized: copies everything whenever something changes.
	// gulp.watch(["src/**/*", "!**/*.ts"], ["static"]);

	// Optimized: only copies the specific file that changed.
	gulp.watch(["src/**/*", "!**/*.ts", "!**/*.less"], (obj) => {
		if (obj.type === "changed") {
			gulp.src(obj.path, { base: "src" })
				.pipe(gulp.dest("dist"));
		}
	});
});
